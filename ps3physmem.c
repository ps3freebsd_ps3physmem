/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/bus.h>
#include <sys/conf.h>
#include <sys/kernel.h>
#include <sys/mman.h>
#include <sys/module.h>
#include <sys/systm.h>
#include <sys/uio.h>

#include <vm/pmap.h>

#include <machine/pmap.h>

#include <powerpc/ps3/ps3-hvcall.h>

#define PHYSMEMSZ	(16 * 1024 * 1024)	/* must be multiple of page size */
#define PAGESZ		(24)			/* 16Mb log2(page size) */

static int ps3physmem_rdwr(struct cdev *dev, struct uio *uio, int ioflag);
static int ps3physmem_mmap(struct cdev *dev, vm_ooffset_t offset,
    vm_paddr_t *paddr, int nprot, vm_memattr_t *memattr);

static int ps3physmem_start = 0;
TUNABLE_INT("ps3physmem.start", &ps3physmem_start);

static int ps3physmem_size = PHYSMEMSZ;
TUNABLE_INT("ps3physmem.size", &ps3physmem_size);

static int ps3physmem_pagesize = PAGESZ;
TUNABLE_INT("ps3physmem.pagesize", &ps3physmem_pagesize);

static struct cdevsw ps3physmem_cdevsw = {
	.d_version	= D_VERSION,
	.d_read		= ps3physmem_rdwr,
	.d_write	= ps3physmem_rdwr,
	.d_mmap		= ps3physmem_mmap,
	.d_name		= "ps3physmem",
};

static uint64_t ps3physmem_paddr;
static void *ps3physmem_vaddr;
static struct cdev *ps3physmem_cdev;

static int
ps3physmem_rdwr(struct cdev *dev, struct uio *uio, int ioflag)
{
	int len;

	if (uio->uio_offset >= ps3physmem_size)
		return (EINVAL);

	len = MIN(uio->uio_resid, ps3physmem_size - uio->uio_offset);

	return (uiomove((char *) ps3physmem_vaddr + uio->uio_offset, len, uio));
}

static int
ps3physmem_mmap(struct cdev *dev, vm_ooffset_t offset,
    vm_paddr_t *paddr, int nprot, vm_memattr_t *memattr)
{
	if (offset >= ps3physmem_size)
		return (EINVAL);

	*paddr = ps3physmem_paddr + offset;
	*memattr = VM_MEMATTR_UNCACHEABLE;

	return (0);
}

static int
ps3physmem_modevent(module_t mod, int type, void *arg)
{
	int err;

	switch (type) {
	case MOD_LOAD:
		/* Create memory region which spans the entire physical memory */

		err = lv1_undocumented_function_114(ps3physmem_start /* start address */,
		    ps3physmem_pagesize /* log2(page size) */,
		    ps3physmem_size /* length */, &ps3physmem_paddr);
		if (err) {
			printf("ps3physmem: Failed to create physical memory region\n");
			return (ENXIO);
		}

		/* Map memory region into kernel space */

		ps3physmem_vaddr = pmap_mapdev(ps3physmem_paddr, ps3physmem_size);
		if (!ps3physmem_vaddr) {
			printf("ps3physmem: Failed to map physical memory region\n");
			lv1_undocumented_function_115(ps3physmem_paddr);
			return (ENXIO);
		}

		/* Create char device */

		ps3physmem_cdev = make_dev(&ps3physmem_cdevsw, 0,
		    UID_ROOT, GID_WHEEL, 0600, "ps3physmem");
		if (!ps3physmem_cdev) {
			printf("ps3physmem: Failed to create /dev/ps3physmem\n");
			pmap_unmapdev((vm_offset_t) ps3physmem_vaddr, ps3physmem_size);
			lv1_undocumented_function_115(ps3physmem_paddr);
			return (ENOMEM);
		}
	break;
	case MOD_UNLOAD:
		/* Destroy char device */

		if (ps3physmem_cdev)
			destroy_dev(ps3physmem_cdev);

		/* Unmap memory region */

		if (ps3physmem_vaddr)
			pmap_unmapdev((vm_offset_t) ps3physmem_vaddr, ps3physmem_size);

		/* Destroy memory region */

		if (ps3physmem_paddr)
			lv1_undocumented_function_115(ps3physmem_paddr);
	break;
	case MOD_SHUTDOWN:
	break;
	default:
		return (EOPNOTSUPP);
	}

	return (0);
}

DEV_MODULE(ps3physmem, ps3physmem_modevent, NULL);
MODULE_VERSION(ps3physmem, 1);
